<?php
/**
 * Created by PhpStorm.
 * User: zhoulongtao
 * Date: 2021-03-03
 * Time: 15:33
 */

if (!function_exists('tobyCreatePayPassword')) {

    /**
     * 生成支付密码
     * @param $pwd
     * @return string
     */
    function tobyCreatePayPassword($pwd)
    {
        return base64_encode("jycard" . md5($pwd) . "jycoin");
    }

}

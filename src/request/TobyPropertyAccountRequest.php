<?php
namespace toby\request;

use toby\request\extend\RequestConfig;
use toby\request\interfaces\Request;

/**
 * 获取可用于物业缴费订单余额
 *
 * @Author zhoulongtao email1946367301
 * @DateTime 2021-03-04
 * 
 */
class TobyPropertyAccountRequest extends RequestConfig implements Request
{
    
    /**
     * @var array 版本路径列表
     */
    protected $methodNameList = [
        'default' => 'fortune/propertyAccount',
        'v1' => 'v1/fortune/propertyAccount',
        'v2' => 'v2/fortune/propertyAccount',
    ];
    /**
     * 排序
     *
     * @var string
     * @Author zhoulongtao email1946367301
     * @DateTime 2021-03-04
     */
    protected $sort = 'customid';

}




<?php
/**
 * Created by PhpStorm.
 * User: zhoulongtao
 * Date: 2021-03-03
 * Time: 14:43
 */

namespace toby\request;

use toby\request\extend\RequestConfig;
use toby\request\interfaces\Request;

class TobyGetAccountRequest extends RequestConfig implements Request
{
    //获取余额
    /**
     * @var array 版本路径列表
     */
    protected $methodNameList = [
        'default' => 'fortune/getAccount',
        'v1' => 'v1/fortune/getAccount',
        'v2' => 'v2/fortune/getAccount',
    ];
    protected $sort = 'customid';

}
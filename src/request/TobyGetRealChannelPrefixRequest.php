<?php
/**
 * Created by PhpStorm.
 * User: zhoulongtao
 * Date: 2021-03-03
 * Time: 14:43
 */

namespace toby\request;

use toby\request\extend\RequestConfig;
use toby\request\interfaces\Request;

class TobyGetRealChannelPrefixRequest extends RequestConfig implements Request
{
    //获取原渠道号
    /**
     * @var array 版本路径列表
     */
    protected $methodNameList = [
        'default' => 'interiorCollocation/getRealChannelPrefix',
        'v1' => 'v1/interiorCollocation/getRealChannelPrefix',
        'v2' => 'v2/interiorCollocation/getRealChannelPrefix',
    ];
    protected $sort = 'current_channel.paytime';

}
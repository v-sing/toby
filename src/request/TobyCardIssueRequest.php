<?php
namespace toby\request;

use toby\request\extend\RequestConfig;
use toby\request\interfaces\Request;

/**
 * 通宝1.0发行
 *
 * @Author zhoulongtao email1946367301
 * @DateTime 2021-03-04
 * 
 */
class TobyCardIssueRequest extends RequestConfig implements Request
{
    
    /**
     * @var array 版本路径列表
     */
    protected $methodNameList = [
        'default' => 'fortune/eCardIssue',
        'v1' => 'v1/fortune/eCardIssue',
        'v2' => 'v2/fortune/eCardIssue',
    ];
    /**
     * 排序
     *
     * @var string
     * @Author zhoulongtao email1946367301
     * @DateTime 2021-03-04
     */
    protected $sort = 'customid.coinAmount.name.personid.orderAmount.mobile.panterid.orderid.sourceCode.info';

}




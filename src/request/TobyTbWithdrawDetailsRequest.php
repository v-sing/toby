<?php
namespace toby\request;

use toby\request\extend\RequestConfig;
use toby\request\interfaces\Request;

/**
 * 通宝2.0发行
 *
 * @Author zhoulongtao email1946367301
 * @DateTime 2021-03-04
 * 
 */
class TobyTbWithdrawDetailsRequest extends RequestConfig implements Request
{
    
    /**
     * @var array 版本路径列表
     */
    protected $methodNameList = [
        'default' => 'tongbao/tbWithdrawDetails',
        'v1' => 'v1/fortune/withdraw',
        'v2' => 'v2/fortune/withdraw',
    ];
    /**
     * 排序
     *
     * @var string
     * @Author zhoulongtao email1946367301
     * @DateTime 2021-03-04
     */
    protected $sort = 'member_id.source_order.source_code.trigger_rules.recharge_amount';

}




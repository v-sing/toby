<?php 

namespace toby\request;

use toby\request\extend\RequestConfig;
use toby\request\interfaces\Request;

/**
 * 实名认证后更改通宝过期时间
 *
 * @Description
 * @example
 * @author 周龙涛
 * @since
 * @date 2021-07-12
 */
class TobyChangeExpireTimeRequest extends RequestConfig implements Request
{
    
    /**
     * @var array 版本路径列表
     */
    protected $methodNameList = [
        'default' => 'fortune/changeExpireTime',
        'v1' => 'v1/fortune/changeExpireTime',
        'v2' => 'v2/fortune/changeExpireTime',
    ];
    /**
     * 排序
     *
     * @var string
     * @Author zhoulongtao email1946367301
     * @DateTime 2021-03-04
     */
    protected $sort = 'customid';


}

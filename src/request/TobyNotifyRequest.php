<?php
namespace toby\request;

use toby\request\extend\RequestConfig;
use toby\request\interfaces\Request;

/**
 * 外部调用队列
 *
 * @Description
 * @example
 * @author 周龙涛
 * @since
 * @date 2021-10-14
 */
class TobyNotifyRequest extends RequestConfig implements Request
{
    
    /**
     * @var array 版本路径列表
     */
    protected $methodNameList = [
        'default' => 'interiorCollocation/notify',
        'v1' => 'v1/interiorCollocation/notify',
        'v2' => 'v2/interiorCollocation/notify',
    ];
    /**
     * 排序
     *
     * @var string
     * @Author zhoulongtao email1946367301
     * @DateTime 2021-03-04
     */
    protected $sort = 'notify_url';

}




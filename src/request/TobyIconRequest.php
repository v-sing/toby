<?php
namespace toby\request;

use toby\request\extend\RequestConfig;
use toby\request\interfaces\Request;

/**
 * 获取icon列表
 *
 * @Description
 * @example
 * @author 周龙涛
 * @since
 * @date 2021-10-14
 */
class TobyIconRequest extends RequestConfig implements Request
{
    
    /**
     * @var array 版本路径列表
     */
    protected $methodNameList = [
        'default' => 'interiorCollocation/getIconList',
        'v1' => 'v1/interiorCollocation/getIconList',
        'v2' => 'v2/interiorCollocation/getIconList',
    ];
    /**
     * 排序
     *
     * @var string
     * @Author zhoulongtao email1946367301
     * @DateTime 2021-03-04
     */
    protected $sort = 'customid.icon_channel';

}




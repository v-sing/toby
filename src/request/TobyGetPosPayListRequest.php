<?php
namespace toby\request;

use toby\request\extend\RequestConfig;
use toby\request\interfaces\Request;

/**
 * 获取总订单记录
 *
 * @Author zhoulongtao email1946367301
 * @DateTime 2021-03-04
 * 
 */
class TobyGetPosPayListRequest extends RequestConfig implements Request
{
    
    /**
     * @var array 版本路径列表
     */
    protected $methodNameList = [
        'default' => 'fortune/getPosPayList',
        'v1' => 'v1/fortune/getPosPayList',
        'v2' => 'v2/fortune/getPosPayList',
    ];
    /**
     * 排序
     *
     * @var string
     * @Author zhoulongtao email1946367301
     * @DateTime 2021-03-04
     */
    protected $sort = '';

}




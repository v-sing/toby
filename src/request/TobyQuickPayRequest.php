<?php
namespace toby\request;

use toby\request\extend\RequestConfig;
use toby\request\interfaces\Request;

/**
 * 通宝线上支付
 *
 * @Author zhoulongtao email1946367301
 * @DateTime 2021-03-04
 * 
 */
class TobyQuickPayRequest extends RequestConfig implements Request
{
    
    /**
     * @var array 版本路径列表
     */
    protected $methodNameList = [
        'default' => 'Tongbao/tbUserNewConsume',
        'v1' => 'v1/fortune/quickPay',
        'v2' => 'v2/fortune/quickPay',
    ];
    /**
     * 排序
     *
     * @var string
     * @Author zhoulongtao email1946367301
     * @DateTime 2021-03-04
     */
    protected $sort = 'Member_id.order_number.Source_code.consume_amount';

}




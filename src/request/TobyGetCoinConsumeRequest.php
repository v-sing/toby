<?php
  
  
  namespace toby\request;
  
  
  use toby\request\extend\RequestConfig;
  use toby\request\interfaces\Request;

  class TobyGetCoinConsumeRequest extends RequestConfig implements Request
  {
    //获取通宝消费数据
    /**
     * @var array 版本路径列表
     */
    protected $methodNameList = [
      'default' => 'fortune/getCoinConsumeOrder',
      'v1' => 'v1/fortune/getCoinConsumeOrder',
      'v2' => 'v2/fortune/getCoinConsumeOrder',
    ];
    protected $sort = 'startdate.enddate.panterid.sourcecode.type';
    
  }
